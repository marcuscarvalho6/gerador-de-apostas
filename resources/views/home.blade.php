@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Minhas Apostas</div>

                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Jogo</th>
                                <th>Números</th>
                                <th>Criado em</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if($user_bets)
                            @foreach($user_bets as $bet)
                            <tr>
                                <td>{{ \Config::get('bets')[$bet['bet']]['name'] }}</td>
                                <td>{{ implode(' - ',$bet['numbers']) }}</td>
                                <td>{{ \Carbon\Carbon::parse($bet['created_at']['date'])->format('Y-m-d H:i:s') }}</td>
                            </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
