@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Gerar Aposta</div>

                    <div class="panel-body">
                        {{ Form::open(['url' => route('generate'),'method' => 'POST']) }}
                        <div class="row">
                            <div class="col-md-12 form-group">
                                {{ Form::label('Jogo') }}
                                {{ Form::select('bet',$bets_available,null,['class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                {{ Form::label('Números') }}
                                {{ Form::select('size',[6 => 6, 7 => 7, 8 => 8 , 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15],null,['class' => 'form-control']) }}
                            </div>
                            <div class="col-md-6 form-group">
                                {{ Form::label('Quantidade de Jogos') }}
                                {{ Form::select('qty',[1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5],null,['class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                {{ Form::submit('Gerar Números',['class' => 'btn btn-primary']) }}
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
