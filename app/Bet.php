<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bet extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id','bet','numbers'];

    protected $dates = ['created_at','updated_at','deleted_at'];
}
