<?php
/**
 * Created by PhpStorm.
 * User: vinicius
 * Date: 17/10/16
 * Time: 18:46
 */

namespace App\Services;


use App\Services\Abstracts\AbstractService;

class BetService extends AbstractService
{
    public function bet_generate($data){

        try{


            $bet = \Config::get('bets')[$data['bet']];

            while($data['qty']){

                $file_content = file_get_contents(storage_path("bets.json"));
                $json_content = json_decode($file_content,true);

                if(!isset($json_content['bets'])){

                    $json_content = Array();
                    $json_content['bets'] = [];
                }

                if(!isset($json_content['bets']['user_id'])){

                    $json_content['bets']['user_id'] = [];
                }

                if(empty($json_content['bets']['user_id'][\Auth::user()->id])){

                    $json_content['bets']['user_id'][\Auth::user()->id] = [];
                }

                $n = [];

                for($i = 0;$i < $data['size'];$i++){

                    $n[] = str_pad(rand(1, $bet['range']), 2, '0', STR_PAD_LEFT);
                }

                sort($n);

                $array_data = [

                    'bet'           => $data['bet'],
                    'numbers'       => $n,
                    'created_at'    => \Carbon::now()
                ];

                array_push($json_content['bets']['user_id'][\Auth::user()->id], $array_data);

                $file = fopen(storage_path("bets.json"), "w");
                fwrite($file, json_encode($json_content));
                fclose($file);

                $data['qty']--;
            }

            return $this->success(null);
        }

        catch(Exception $e){

            return $this->error(null,$e->getMessage());
        }
    }

    public function get_bets(){

        $file_content = file_get_contents(storage_path("bets.json"));

        $array_content = json_decode($file_content,true);

        $user_bets = $array_content['bets']['user_id'][\Auth::user()->id];

        return $this->success($user_bets);
    }
}