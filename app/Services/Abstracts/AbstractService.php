<?php
/**
 * Created by PhpStorm.
 * User: vinicius
 * Date: 17/10/16
 * Time: 18:43
 */

namespace App\Services\Abstracts;


abstract class AbstractService
{

    private $error = false;
    private $msg = "Solicitação concluída com sucesso";

    public function error($data,$msg = null){

        $this->error = true;
        $this->msg = "Falha na solicitação";

        if($msg)
            $this->msg = $msg;

        return array('error' => $this->error, 'data' => $data,'msg' => $this->msg);

    }

    public function success($data,$msg = null){

        if($msg)
            $this->msg = $msg;

        return array('error' => $this->error, 'data' => $data, 'msg' => $this->msg);

    }

}