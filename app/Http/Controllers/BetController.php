<?php

namespace App\Http\Controllers;

use App\Services\BetService;
use Illuminate\Http\Request;

use App\Http\Requests;

class BetController extends Controller
{
    private $bet_service;


    public function __construct()
    {

        $this->bet_service = new BetService();
    }

    public function index(){

        $user_bets = $this->bet_service->get_bets();

        return view('home')
            ->with('user_bets',$user_bets['data']);
    }

    public function create(){

        $bets_available = [];

        foreach(\Config::get('bets') as $key=>$value){

            $bets_available[$key] = $value['name'];
        }

        return view('generate')
            ->with('bets_available',$bets_available);
    }

    public function store(Request $request){

        $data = $request->all();

        $bet_generate_response = $this->bet_service->bet_generate($data);

        if($bet_generate_response['error']){

            return redirect()->back()->with('error',$bet_generate_response['msg']);
        }

        return redirect()->route('home')
            ->with('success',$bet_generate_response['msg'])
            ->with('bets',$bet_generate_response['data']);
    }

    public function get_range($id){

        return \Config::get('bets')[$id];
    }

}
