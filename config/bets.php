<?php

return [

        1 => [
            'id'        => 1,
            'name'      => 'Mega Sena',
            'min_size'  => 6,
            'max_size'  => 15,
            'range'     => 60
        ],

        2 => [

            'id'        => 2,
            'name'      => 'Quina',
            'min_size'  => 5,
            'max_size'  => 15,
            'range'     => 80
        ],

        3 => [

            'id'        => 3,
            'name'      => 'Lotomania',
            'min_size'  => 80,
            'max_size'  => 80,
            'range'     => 100
        ],

        4 => [

            'id'        => 4,
            'name'      => 'Lotofácil',
            'min_size'  => 15,
            'max_size'  => 18,
            'range'     => 25
        ],
];