<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();


Route::group(['middleware' => 'auth'],function(){

    Route::get('/home',                 ['uses' => 'BetController@index',           'as' => 'home']);
    Route::get('/generate',             ['uses' => 'BetController@create',          'as' => 'generate']);
    Route::post('/generate',            ['uses' => 'BetController@store',           'as' => 'generate']);
    Route::get('/get_range/{id}',            ['uses' => 'BetController@get_range',       'as' => 'get_range']);
});


